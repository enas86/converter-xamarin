﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Net.Http;
using System.Text;

namespace Converter
{
    class ApplicationViewModel : INotifyPropertyChanged
    {
        public ObservableCollection<ValuteModel> Valutes { get; set; }
        private bool stop;
        private DateTime date;
        public DateTime Date
        {
            get { return date; }
            set
            {
                if (date != value)
                {
                    date = value;
                    LoadInfo();
                    OnPropertyChanged("Date");
                }
            }
        }
        private string outDate;
        public string OutDate
        {
            get { return outDate; }
            set 
            {
                outDate = value;
                OnPropertyChanged("OutDate");
            }
        }
        private double value1;
        public double Value1
        {
            get { return value1; }
            set 
            { 
                value1 = value;
                calc1();
                OnPropertyChanged("Value1");
                stop = false;
            }
        }
        private double value2;
        public double Value2
        {
            get { return value2; }
            set 
            { 
                value2 = value;
                calc2();
                OnPropertyChanged("Value2");
                stop = false;
            }
        }
        private ValuteModel valute1;
        public ValuteModel Valute1
        {
            get { return valute1; }
            set
            {
                valute1 = value;
                calc1();
                OnPropertyChanged("Valute1");
                stop = false;
            }
        }
        private ValuteModel valute2;
        public ValuteModel Valute2
        {
            get { return valute2; }
            set
            {
                valute2 = value;
                calc2();
                OnPropertyChanged("Valute2");
                stop = false;
            }
        }
        private void calc1() 
        {
            if(!stop && valute1 != null && valute2 != null)
            {
                stop = true;
                value2 = valute1.Value * value1 / valute2.Value * valute2.Nominal;
                Value2 = Math.Round(value2, 2);
            }
        }
        private void calc2()
        {
            if (!stop && valute1 != null && valute2 != null)
            {
                stop = true;
                value1 = valute2.Value * value2 / valute1.Value * valute1.Nominal;
                Value1 = Math.Round(value1, 2);
            }
        }
        private async void LoadInfo()
        {
            try
            {
                string url = "";
                url = $"https://www.cbr-xml-daily.ru/archive/{Date.Year}/{Date.Month}/{Date.Day}/daily_json.js";

                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(url);
                var response = await client.GetAsync(client.BaseAddress);

                DateTime tempDate = date;
                while (response.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    tempDate = tempDate.AddDays(-1); 

                    url = $"https://www.cbr-xml-daily.ru/archive/{tempDate.Year}/{tempDate.Month}/{tempDate.Day}/daily_json.js";
                    response = await client.GetAsync(url);
                }
                OutDate = $"{tempDate.Day}/{tempDate.Month}/{tempDate.Year}";

                ValuteModel tempVal1 = valute1;
                ValuteModel tempVal2 = valute2;
                double tempVal = value1;
                Valutes.Clear();
                var content = await response.Content.ReadAsStringAsync();
                JObject jObject = JObject.Parse(content);
                var valutes = jObject.SelectToken(@"$.Valute").Values();
                Valutes.Add(new ValuteModel {
                    CharCode = "RUB",
                    Nominal = 1,
                    Name = "Российский рубль",
                    Value = (float)1,
                });
                foreach (var temp in valutes)
                {
                    ValuteModel valute = JsonConvert.DeserializeObject<ValuteModel>(temp.ToString());
                    Valutes.Add(valute);
                }

                stop = true;
                foreach(ValuteModel temp in Valutes)
                {
                    if (temp.CharCode.Equals(tempVal1.CharCode)) { Valute1 = temp; stop = true; }
                    if (temp.CharCode.Equals(tempVal2.CharCode)) Valute2 = temp;
                }
                Value1 = tempVal;
            }
            catch (Exception ex)
            { }
        }

        public ApplicationViewModel()
        {
            Valutes = new ObservableCollection<ValuteModel>();
            date = DateTime.Now.Date;
            LoadInfo();
            stop = false;
            value1 = value2 = 0;
            valute1 = valute2 = null;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
        }
    }
}
